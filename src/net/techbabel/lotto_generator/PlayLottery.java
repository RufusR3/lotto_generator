package net.techbabel.lotto_generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayLottery {
	private final int minBalls;
	private final int maxBalls;
	private final int numBallsPulled;
	private final int minExtraBall;
	private final int maxExtraBall;

	private final StringBuilder thePlay = new StringBuilder();
	private final List<Integer> thisDrawing = new ArrayList<Integer>();

	public PlayLottery(final Lottery theLottery) {
		switch (theLottery) {
		case MEGA_MILLIONS:
			this.minBalls = Lottery.MEGA_MILLIONS.getMinBalls();
			this.maxBalls = Lottery.MEGA_MILLIONS.getMaxBalls();
			this.numBallsPulled = Lottery.MEGA_MILLIONS.getNumBallsPulled();
			this.minExtraBall = Lottery.MEGA_MILLIONS.getMinExtraBall();
			this.maxExtraBall = Lottery.MEGA_MILLIONS.getMaxExtraBall();
			break;

		case POWERBALL:
			this.minBalls = Lottery.POWERBALL.getMinBalls();
			this.maxBalls = Lottery.POWERBALL.getMaxBalls();
			this.numBallsPulled = Lottery.POWERBALL.getNumBallsPulled();
			this.minExtraBall = Lottery.POWERBALL.getMinExtraBall();
			this.maxExtraBall = Lottery.POWERBALL.getMaxExtraBall();
			break;

		default:
			this.minBalls = Lottery.POWERBALL.getMinBalls();
			this.maxBalls = Lottery.POWERBALL.getMaxBalls();
			this.numBallsPulled = Lottery.POWERBALL.getNumBallsPulled();
			this.minExtraBall = Lottery.POWERBALL.getMinExtraBall();
			this.maxExtraBall = Lottery.POWERBALL.getMaxExtraBall();
			break;
		}

		final LottoMachine balls = new LottoMachine(this.minBalls, this.maxBalls);
		final LottoMachine extraBalls = new LottoMachine(this.minExtraBall, this.maxExtraBall);

		final List<Integer> theDrawing = new ArrayList<Integer>();

		for (int theDraw = 1; theDraw <= numBallsPulled; theDraw++) {
			final int theBall = balls.drawBall();
			theDrawing.add(theBall);
		}
		
		this.thisDrawing.addAll(theDrawing);

		Collections.sort(theDrawing);

		final int theExtraBall = extraBalls.drawBall();
		this.thisDrawing.add(theExtraBall);

		for (final Integer ball : theDrawing) {
			this.thePlay.append(ball);
			this.thePlay.append(" ");
		}

		this.thePlay.append("[");
		this.thePlay.append(theExtraBall);
		this.thePlay.append("]");

	}

	public final String getThePlay() {
		return this.thePlay.toString();
	}
	
	public final List<Integer> getThisDrawing() {
		return this.thisDrawing;
	}
}
