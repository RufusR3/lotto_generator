package net.techbabel.lotto_generator;

import java.util.ArrayList;
import java.util.List;

public class BallSet {
	private final List<Integer> ballSet;

	public BallSet(final int minValue, final int maxValue) {
		this.ballSet = new ArrayList<Integer>();

		for (int ballNumber = minValue; ballNumber <= maxValue; ballNumber++) {
			this.ballSet.add(this.ballSet.size(), ballNumber);
		}
	}

	public final List<Integer> getBallSet() {
		return this.ballSet;
	}
}
