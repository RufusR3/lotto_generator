package net.techbabel.lotto_generator;

public class NewPowerballGame {

	public static void main(String[] args) throws Exception {
		if (args.length < 1 || args.length > 1) {
			throw new Exception("Invalid number of arguments.");
		}
		
		for (int x = 1; x <= Integer.valueOf(args[0]); x++) {
			final PlayLottery playGame = new PlayLottery(Lottery.POWERBALL);
			System.out.println(playGame.getThePlay());
		}
	}

}
