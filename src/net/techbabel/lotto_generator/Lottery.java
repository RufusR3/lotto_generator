package net.techbabel.lotto_generator;

public enum Lottery {
	MEGA_MILLIONS(1, 70, 5, 1, 25), POWERBALL(1, 69, 5, 1, 26);

	private final int minBalls;
	private final int maxBalls;
	private final int minExtraBall;
	private final int maxExtraBall;
	private final int numBallsPulled;
	
	private static final int DEFAULT_NUM_BALLS_DRAWN = 0;

	Lottery(final int minBalls, final int maxBalls) {
		this.minBalls = minBalls;
		this.maxBalls = maxBalls;
		this.numBallsPulled = DEFAULT_NUM_BALLS_DRAWN;
		this.minExtraBall = 0;
		this.maxExtraBall = 0;
	}

	Lottery(final int minBalls, final int maxBalls, final int numBallsDrawn, final int minExtraBall, final int maxExtraBall) {
		this.minBalls = minBalls;
		this.maxBalls = maxBalls;
		this.numBallsPulled = numBallsDrawn;
		this.minExtraBall = minExtraBall;
		this.maxExtraBall = maxExtraBall;
	}

	public final int getMinBalls() {
		return this.minBalls;
	}

	public final int getNumBallsPulled() {
		return this.numBallsPulled;
	}
	
	public final int getMaxBalls() {
		return this.maxBalls;
	}

	public final int getMinExtraBall() {
		return this.minExtraBall;
	}

	public final int getMaxExtraBall() {
		return this.maxExtraBall;
	}
}
