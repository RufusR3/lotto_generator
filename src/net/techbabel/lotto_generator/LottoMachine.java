package net.techbabel.lotto_generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.techbabel.gametools.common.random.RandomNumber;

public class LottoMachine {
	private final List<Integer> balls = new ArrayList<Integer>();

	public LottoMachine(final int minValue, final int maxValue) {
		final BallSet ballSet = new BallSet(minValue, maxValue);
		this.balls.addAll(ballSet.getBallSet());
	}

	public final int drawBall() {
		Collections.shuffle(this.balls, RandomNumber.getRandomGenerator());

		final int ballPoolSize = this.balls.size();
		final int ballPosition = RandomNumber.generate(0, ballPoolSize - 1);
		final int theBall = balls.remove(ballPosition);

		return theBall;
	}
}
